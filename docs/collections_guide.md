# Creating a Collection

## What is a collection in Āhau?

A collection is a way of grouping and finding related stories. You may want to group stories by the original knowledge holder, (i.e Stories told by Koro), or by a category (stories relating to the whenua).
## Creating a new collection

To create a new Collection navigate to the *Archive* page of either your personal profile of tribal profile that you’re a member of. Do this by clicking on *Archive* on the side panel.

<img src="_media/2.png" width="580" height="auto"/>

Select *Create a New Collection* by clicking on it, a box will appear where you are able to add details about your new collection.  

<img src="_media/4.png" width="580" height="auto"/>

### Add Collection Details

Add details about your new Collection (i.e. name, description and an image related to the collection), and select whether this collection will be for your private archive or for your tribal archive by clicking on the *Private* button (see image below). 

<img src="_media/4.png" width="580" height="auto">
<br>
<img src="_media/5.png" width="580" height="auto">

You'll then see a dropdown list of several different options to select either *Private*, or the different tribes you are a member of. If you are not yet connected to any tribes or communities, you'll only have the option to select *Private*.  
### Who can see my collections?

- Private: only you will be able to view your private collection.
- Tribe: members within your tribe will be able to view your collection and all information associated with it. NB: Only the person who creates the Collection (The Kaitikai) can edit, add stories to, or delete a collection.
## Adding Stories to a New Collection

<img src="_media/6.png" width="580" height="auto">

When you've finished adding details about your new collection, you can add stories that are associated with the collection. To do this click the *+ Stories* button (see image below), you'll then see a list of stories that you have already created in your personal Āhau profile, or stories that have been created by your Tribe.

<img src="_media/7.png" width="580" height="auto">

Select related stories to add them to your collection by clicking on them. The list of stories populated will be based on the existing stories within your personal or tribal archive. 

<img src="_media/8.png" width="580" height="auto">

You will now be able to view your new collection in your personal or tribal archive by navigating to the *Archive* page.

<img src="_media/9.png" width="580" height="auto">

You may want to add additional stories to your collection to make the collection more detailed, as over time several users may add new stories to the tribal archive which can be connected through a collection. To keep your collections up to date with relevant information and new stories, use the following instructions to add more stories to an existing collection.
### When You're Viewing a Collection

Navigate to the collection you want to add new stories to. To do this go to the Archive page in either your personal profile or tribal profile. 

Click on the blue pen	icon on the Collection to edit the collection. 

<img src="_media/10.png" width="580" height="auto">

A pop-up window will appear with your pre-populated collection information.

Click on the *+ Stories* button.

<img src="_media/11.png" width="580" height="auto">

You will see a list of stories in your archive, select the stories that you want to add to the collection. Click the tick button to save.

<img src="_media/12.png" width="580" height="auto">

### When you're viewing a Story

You can also add a story to a collection when you're viewing a story. To do this, click on the story from your personal or tribal Archives page. (Note that you can only edit, add or delete a Tribal story if you created the story, i.e. if you are the Tribal Kaitiaki). 

<img src="_media/13.png" width="580" height="auto">

When the story has opened, click on the 'Edit Story' button in the bottom-right corner.

<img src="_media/14.png" width="580" height="auto">

Scroll down until you see the Collections section, and click the '+ Collection' button to connect the collection with the story. This story will now be associated with the Collection. 

<img src="_media/15.png" width="580" height="auto">

Visit the collection archive page to view the updated collection with the newly added stories. 

<img src="_media/16.png" width="580" height="auto">

Once complete, you'll be able to see the new stories in your Collection Archive.
## Removing Stories from a Collection

### When you're vieiwing a Collection

Navigate to the Archive page and click on the blue pen button to edit  your collection.

To remove stories from your collection, click the ‘x’ button in the top right-hand corner of the story. 

<img src="_media/17.png" width="580" height="auto">

Click the tick button to save your changes.

### When you're viewing a Story

You can also delete a story from a collection when you're editing a story. To do this, navigate into your collection and click on the story you want to remove from a collection. 

<img src="_media/18.png" width="580" height="auto">

Click on the 'Edit Story' button in the bottom-right corner. 

<img src="_media/19.png" width="580" height="auto">

Scroll down until you see the Collections section, and click the 'x' button to remove the collection. This story will no longer be associated with the Collection. 
 
<img src="_media/20.png" width="580" height="auto">

## Editing a Collection

<img src="_media/21.png" width="580" height="auto">

To edit a collection, click the blue pen icon next to the collection title. From there a pop-up window will open where you can edit the name, description and image associated with the collection. You can also add and remove stories. 

Click the tick button when you’re done editing to save your changes. 